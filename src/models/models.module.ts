import { Module } from '@nestjs/common';
import { ModelsService } from './models.service';

@Module({
  providers: [ModelsService]
})
export class ModelsModule {}
